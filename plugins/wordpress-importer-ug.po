# Translation of Development (trunk) in Uighur
# This file is distributed under the same license as the Development (trunk) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2012-05-18 08:17:49+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/1.0-alpha-1000\n"
"Project-Id-Version: Development (trunk)\n"

#: wordpress-importer.php:1017
msgid "Choose a WXR (.xml) file to upload, then click Upload file and import."
msgstr ""

#: wordpress-importer.php:889
msgid "Zero size file downloaded"
msgstr "چوڭلۇقى نۆل بولغان ھۆججەت چۈشۈرۈلدى"

#: wordpress-importer.php:877
msgid "Remote server returned error response %1$d %2$s"
msgstr "يىراقتىكى مۇلازىمېتىر خاتالىق ئىنكاسى %1$d %2$s نى قايتۇردى"

#: wordpress-importer.php:235
msgid "Failed to import author %s. Their posts will be attributed to the current user."
msgstr ""

#: wordpress-importer.php:262
msgid "To make it easier for you to edit and save the imported content, you may want to reassign the author of the imported item to an existing user of this site. For example, you may want to import all the entries as <code>admin</code>s entries."
msgstr ""

#: wordpress-importer.php:522
msgid "Failed to import &#8220;%s&#8221;: Invalid post type %s"
msgstr "&#8220;%s&#8221;نى ئىمپورتلاشتا خاتالىق كۆرۈلدى: يازما تىپى %s ئىناۋەتسىز"

#: wordpress-importer.php:500 wordpress-importer.php:626
msgid "Failed to import %s %s"
msgstr "%s %s نى ئىمپورتلاشتا خاتالىق كۆرۈلدى"

#: wordpress-importer.php:418
msgid "Failed to import category %s"
msgstr "كاتېگورىيە %s نى ئىمپورتلاشتا خاتالىق كۆرۈلدى"

#: wordpress-importer.php:456
msgid "Failed to import post tag %s"
msgstr "يازما خەتكۈچى %s نى ئىمپورتلاشتا خاتالىق كۆرۈلدى"

#: wordpress-importer.php:543
msgid "%s &#8220;%s&#8221; already exists."
msgstr "%s &#8220;%s&#8221; مەۋجۇد."

#: wordpress-importer.php:744
msgid "Menu item skipped due to missing menu slug"
msgstr "تىزىملىكنىڭ باشقا نامى كىرگۈزۈلمىگەچكە ئاتلاپ ئۆتۈلدى"

#: wordpress-importer.php:598
msgid "Failed to import %s &#8220;%s&#8221;"
msgstr "%s &#8220;%s&#8221; نى ئىمپورتلاشتا خاتالىق كۆرۈلدى"

#: wordpress-importer.php:316
msgid "assign posts to an existing user:"
msgstr "يازمىلار مەۋجۇد ئىشلەتكۈچىگە تەقسىملەنسۇن:"

#: wordpress-importer.php:308
msgid "as a new user:"
msgstr "يېڭى ئىشلەتكۈچى سۈپىتىدە:"

#: wordpress-importer.php:210
msgid "This WXR file (version %s) may not be supported by this version of the importer. Please consider updating."
msgstr "بۇ نەشرىدىكى كىرگۈزگۈچ WXR ھۆججىتى (نەشرى %s) ھۆججىتىنى قوللىمايدۇ. يېڭىلاشنى ئويلىشىپ كۆرۈڭ."

#: wordpress-importer.php:264
msgid "If a new user is created by WordPress, a new password will be randomly generated and the new user&#8217;s role will be set as %s. Manually changing the new user&#8217;s details will be necessary."
msgstr "ناۋادا يېڭى ئىشلەتكۈچى ۋوردپرەس تەرىپىدىن قۇرۇلسا، بىر پارول ئاپتوماتىك ھاسىل قىلىنىپ، يېڭى ئىشلەتكۈچى دەرىجىسى %s قىلىپ بەلگىلىنىدۇ. يېڭى ئىشلەتكۈچىنىڭ تەپسىلىي ئۇچۇرلىرىنى قولدا ئۆزگەرتىش زۆرۈر بولىدۇ."

#: wordpress-importer.php:751
msgid "Menu item skipped due to invalid menu slug: %s"
msgstr "تىزىملىكنىڭ باشقا نامى ئىناۋەتسىز بولغاچقا ئاتلاپ ئۆتۈلدى: %s"

#: wordpress-importer.php:814
msgid "Fetching attachments is not enabled"
msgstr "قوشۇمچە ھۆججەتنى ئالغىلى بولمىدى"

#: wordpress-importer.php:179
msgid "Remember to update the passwords and roles of imported users."
msgstr "ئىمپورتلانغان ئىشلەتكۈچىلەرنىڭ پارول ۋە دەرىجىلىرىنى يېڭىلاشنى ئۇنتۇماڭ."

#: wordpress-importer.php:135
msgid "The file does not exist, please try again."
msgstr "ھۆججەت مەۋجۇد ئەمەس. قايتا سىناڭ."

#: parsers.php:67 parsers.php:72 parsers.php:262 parsers.php:451
msgid "This does not appear to be a WXR file, missing/invalid WXR version number"
msgstr "بۇ WXR ھۆججىتى ئەمەسكەن، WXR نەشر نومۇرى خاتا ياكى يوق ئىكەن."

#: parsers.php:43
msgid "Details are shown above. The importer will now try again with a different parser..."
msgstr "تەپسىلاتى يۇقىرىدا كۆرسىتىلگەندەك. كىرگۈزگۈچ ئەمدى ئوخشىمايدىغان پارچىلىغۇچتا قايتا سىناپ كۆرىدۇ..."

#. Description of the plugin/theme
msgid "Import posts, pages, comments, custom fields, categories, tags and more from a WordPress export file."
msgstr "ۋوردپرەس ئېكسپورت ھۆججىتىدىكى يازمىلار، بەتلەر، ئىنكاسلار، ئۆزى بەلگىلىگەن بۆلەكلەر، كاتېگورىيەلەر، خەتكۈچلەر ۋە باشقىلار ئىمپورتلىنىدۇ."

#: wordpress-importer.php:1001
msgid "A new version of this importer is available. Please update to version %s to ensure compatibility with newer export files."
msgstr "بۇ كىرگۈزگۈچنىڭ يېڭى نەشرى چىقتى. يېڭىراق ئېكسپورت ھۆججەتلىرىنى قوللايدىغان %s نەشرىگە يېڭىلىۋېلىڭ."

#: parsers.php:42 parsers.php:63
msgid "There was an error when reading this WXR file"
msgstr "WXR ھۆججىتىنى ئوقۇش جەريانىدا خاتالىق كۆرۈلدى"

#: wordpress-importer.php:305
msgid "or create new user with login name:"
msgstr "ياكى يېڭىدىن ئىشلەتكۈچى قۇرۇڭ:"

#: wordpress-importer.php:369
msgid "Failed to create new user for %s. Their posts will be attributed to the current user."
msgstr "%s ئۈچۈن يېڭىدىن ئىشلەتكۈچى قۇرغىلى بولمىدى. ئۇلارنىڭ يازمىلىرى نۆۋەتتىكى ئىشلەتكۈچىگە تەۋە قىلىنىدۇ."

#: wordpress-importer.php:318
msgid "or assign posts to an existing user:"
msgstr "ياكى يازمىلار مەۋجۇد ئىشلەتكۈچىگە تەقسىملىنىدۇ."

#: wordpress-importer.php:884
msgid "Remote file is incorrect size"
msgstr "يىراقتىكى ھۆججەتنىڭ سىغىمى توغرا ئەمەس"

#. Author URI of the plugin/theme
msgid "http://wordpress.org/"
msgstr "http://wordpress.org/"

#: wordpress-importer.php:277
msgid "Download and import file attachments"
msgstr "ھۆججەت قوشۇمچىلىرىنى چۈشۈرۈش ۋە ئىمپورتلاش"

#. Plugin Name of the plugin/theme
msgid "WordPress Importer"
msgstr "ۋوردپرەس كىرگۈزگۈچى"

#: wordpress-importer.php:281
msgid "Submit"
msgstr "يوللاش"

#: wordpress-importer.php:319
msgid "- Select -"
msgstr "- تاللاڭ -"

#: wordpress-importer.php:895
msgid "Remote file is too large, limit is %s"
msgstr "يىراقتىكى ھۆججەت بەك چوڭ، ئەسلى چەك %s"

#: wordpress-importer.php:178
msgid "All done."
msgstr "ھەممىسى تۈگىدى."

#: wordpress-importer.php:178
msgid "Have fun!"
msgstr "بۇ يەردىن كۆرۈڭ!"

#: wordpress-importer.php:827
msgid "Invalid file type"
msgstr "ھۆججەت تىپى ئىناۋەتسىز"

#: wordpress-importer.php:294
msgid "Import author:"
msgstr "ئاپتورنى ئىمپورتلاش:"

#: wordpress-importer.php:274
msgid "Import Attachments"
msgstr "قوشۇمچە ھۆججەتلەرنى ئىمپورتلاش"

#: wordpress-importer.php:871
msgid "Remote server did not respond"
msgstr "يىراقتىكى مۇلازىمېتىر ئىنكاس قايتۇرمىدى"

#: wordpress-importer.php:1091
msgid "Import <strong>posts, pages, comments, custom fields, categories, and tags</strong> from a WordPress export file."
msgstr "ۋوردپرەس ئېكسپورت ھۆججىتىدىن <strong>يازما، بەت، ئىنكاس، ئۆزى بەلگىلەش بۆلىكى، كاتېگورىيە ۋە خەتكۈچلەر</strong> ئىمپورتلىنىدۇ."

#: wordpress-importer.php:134 wordpress-importer.php:143
#: wordpress-importer.php:194 wordpress-importer.php:202
msgid "Sorry, there has been an error."
msgstr "كەچۈرۈڭ، بۇ يەردە خاتالىق كۆرۈلدى."

#. Plugin URI of the plugin/theme
msgid "http://wordpress.org/extend/plugins/wordpress-importer/"
msgstr "http://wordpress.org/extend/plugins/wordpress-importer/"

#. Author of the plugin/theme
msgid "wordpressdotorg"
msgstr "wordpressdotorg"

#: wordpress-importer.php:261
msgid "Assign Authors"
msgstr "ئاپتورنى بەلگىلەش"

#: wordpress-importer.php:1016
msgid "Howdy! Upload your WordPress eXtended RSS (WXR) file and we&#8217;ll import the posts, pages, comments, custom fields, categories, and tags into this site."
msgstr "ياخشىمۇسىز! ئۆزىڭىزنىڭ ۋوردپرەس كېڭەيتىلگەن RSS (WXR) ھۆججىتىڭىزنى يۈكلەڭ، ئاندىن بىز يازما، بەت، ئىنكاس، ئۆزىڭىز بەلگىلىگەن بۆلەك، كاتېگورىيە ۋە خەتكۈچلەرنى بۇ توربەتكە ئىمپورتلاپ بېرىمىز."

#: wordpress-importer.php:994
msgid "Import WordPress"
msgstr "ۋوردپرەستىن ئىمپورتلاش"